<?php

namespace DicomResearch\ResultBuildBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;

use FOS\RestBundle\Controller\Annotations;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TeamCityController extends FOSRestController
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function postBuildAction(Request $request)
    {
        $this->container->
        get('result_build.result_build.handler')->
        processResultBuild(
            $request->request->all()
        );

        return new Response('ok');
    }
}
