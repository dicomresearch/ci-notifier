<?php

/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 04.08.15
 * Time: 13:40
 */

namespace DicomResearch\ResultBuildBundle\Builder;

use DicomResearch\ResultBuildBundle\BuildResult\BuildResult;
use DicomResearch\ResultBuildBundle\Issue\TicketNumberResolver;

abstract class BuilderAbstract
{
    /**
     * @var BuildResult
     */
    private $resultBuild;

    /**
     * @var TicketNumberResolver
     */
    private $ticketNumberResolver;

    public function __construct(BuildResult $buildResult, TicketNumberResolver $ticketNumberResolver)
    {
        $this->resultBuild = $buildResult;
        $this->ticketNumberResolver = $ticketNumberResolver;
    }

    /**
     * @return BuildResult
     */
    public function getResultBuild()
    {
        return $this->resultBuild;
    }

    /**
     * @return TicketNumberResolver
     */
    public function getTicketNumberResolver()
    {
        return $this->ticketNumberResolver;
    }

    /**
     * @param array $teamCityBuildResult
     *
     * @return BuildResult
     */
    abstract public function create(array $teamCityBuildResult);
}
