<?php
/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 04.08.15
 * Time: 14:27
 */

namespace DicomResearch\ResultBuildBundle\Builder;

use DicomResearch\ResultBuildBundle\Exception\TeamCityBuilderException;

class TeamCityBuilder extends BuilderAbstract
{
    public function create(array $teamCityBuildResult)
    {
        if (!array_key_exists('projectId', $teamCityBuildResult) && $teamCityBuildResult['projectId'] === null) {
            throw TeamCityBuilderException::notFoundParameter('projectId', $teamCityBuildResult);
        }

        if (!array_key_exists('branchDisplayName', $teamCityBuildResult) &&
            $teamCityBuildResult['branchDisplayName'] === null) {
            throw TeamCityBuilderException::notFoundParameter('branchDisplayName', $teamCityBuildResult);
        }

        if (!array_key_exists('buildResult', $teamCityBuildResult) && $teamCityBuildResult['buildResult'] === null) {
            throw TeamCityBuilderException::notFoundParameter('buildResult', $teamCityBuildResult);
        }

        if (!array_key_exists('buildStateDescription', $teamCityBuildResult) &&
            $teamCityBuildResult['buildStateDescription'] === null) {
            throw TeamCityBuilderException::notFoundParameter('buildStateDescription', $teamCityBuildResult);
        }

        if (!array_key_exists('buildStatusHtml', $teamCityBuildResult) &&
            $teamCityBuildResult['buildStatusHtml'] === null) {
            throw TeamCityBuilderException::notFoundParameter('buildStatusHtml', $teamCityBuildResult);
        }

        $this->getResultBuild()->setProjectName($teamCityBuildResult['projectId']);
        $this->getResultBuild()->setBranchName($teamCityBuildResult['branchDisplayName']);
        $this->getResultBuild()->setBuildResult($teamCityBuildResult['buildResult']);
        $this->getResultBuild()->setStateDescription($teamCityBuildResult['buildStateDescription']);
        $this->getResultBuild()->setStatusHtml($teamCityBuildResult['buildStatusHtml']);

        $issueId = $this->getTicketNumberResolver()
            ->getTicketNumberFromBranchName(
                $teamCityBuildResult['branchDisplayName'],
                $teamCityBuildResult['projectId']
            );
        $this->getResultBuild()->setIssueId($issueId);

        return $this->getResultBuild();
    }
}
