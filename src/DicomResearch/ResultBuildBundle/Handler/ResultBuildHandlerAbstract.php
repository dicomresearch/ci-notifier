<?php

namespace DicomResearch\ResultBuildBundle\Handler;

use DicomResearch\ResultBuildBundle\Builder\BuilderAbstract;
use DicomResearch\ResultBuildBundle\BuildResult\BuildResultAction;
use DicomResearch\ResultBuildBundle\BuildResult\ProcessBuildResult;
use DicomResearch\ResultBuildBundle\Client\ClientOfIssueTrackerAbstract;
use DicomResearch\ResultBuildBundle\Exception\ResultBuilderHandlerException;

abstract class ResultBuildHandlerAbstract
{
    /**
     * @var BuilderAbstract
     */
    private $buildResultBuilder;

    /**
     * @var ClientOfIssueTrackerAbstract
     */
    private $issueTrackerClient;

    /**
     * @var ProcessBuildResult
     */
    private $processBuildResult;

    /**
     * @var BuildResultAction
     */
    private $buildResultAction;

    /**
     * @param ClientOfIssueTrackerAbstract $client через какого клиента трекера мы будем работать
     * @param ProcessBuildResult $processBuildResult указывает какие действия выполнить
     *                                               после обработки результатов сборки
     * @param BuildResultAction $buildResultAction кем будем обрабатывать действия,
     *                                             необходимые после обработки результатов
     * @param BuilderAbstract $buildResultBuilder формирование BuildResult из результатов сборки
     */
    public function __construct(
        ClientOfIssueTrackerAbstract $client,
        ProcessBuildResult $processBuildResult,
        BuildResultAction $buildResultAction,
        BuilderAbstract $buildResultBuilder
    ) {
        $this->issueTrackerClient = $client;
        $this->processBuildResult = $processBuildResult;
        $this->buildResultAction = $buildResultAction;
        $this->buildResultBuilder = $buildResultBuilder;
    }

    /**
     * @param BuilderAbstract $builder
     *
     * @return $this
     */
    public function setBuilder(BuilderAbstract $builder)
    {
        $this->buildResultBuilder = $builder;
        return $this;
    }

    /**
     * @return BuilderAbstract
     * @throws ResultBuilderHandlerException
     */
    public function getBuilder()
    {
        if (!$this->buildResultBuilder) {
            throw ResultBuilderHandlerException::builderMustBeDefined();
        }

        return $this->buildResultBuilder;
    }

    /**
     * @return BuildResultAction
     */
    public function getBuildResultAction()
    {
        return $this->buildResultAction;
    }

    /**
     * @return ProcessBuildResult
     */
    public function getProcessBuildResult()
    {
        return $this->processBuildResult;
    }

    /**
     * @param ClientOfIssueTrackerAbstract $client
     *
     * @return ClientOfIssueTrackerAbstract
     */
    public function setClient(ClientOfIssueTrackerAbstract $client)
    {
        $this->issueTrackerClient = $client;
        return $this->issueTrackerClient;
    }

    /**
     * @return ClientOfIssueTrackerAbstract
     */
    public function getClient()
    {
        return $this->issueTrackerClient;
    }

    /**
     * Обработка результата сборки
     *
     * @param array $parameters
     *
     * @return mixed
     */
    abstract public function processResultBuild(array $parameters);
}
