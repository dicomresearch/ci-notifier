<?php

namespace DicomResearch\ResultBuildBundle\Handler;

class TeamCityResultBuildHandler extends ResultBuildHandlerAbstract
{
    /**
     * @param array $buildResultParameters
     *
     * @return void
     * @throws \DicomResearch\ResultBuildBundle\Exception\ResultBuilderHandlerException
     */
    public function processResultBuild(array $buildResultParameters)
    {
        $buildResult = $this->getBuilder()->create($buildResultParameters);

        $actions = $this->getProcessBuildResult()->getActionsAtBuildResult($buildResult);

        $this->getBuildResultAction()->doActionsAfterBuilds($actions, $buildResult);
    }
}
