<?php
/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 07.08.15
 * Time: 17:22
 */

namespace DicomResearch\ResultBuildBundle\BuildResult;

use DicomResearch\ResultBuildBundle\Client\ClientOfIssueTrackerAbstract;
use DicomResearch\ResultBuildBundle\Exception\BuildResultActionsException;

/**
 * Класс, отвечающий за обработку экшенов
 * после результатов сборки на CI
 *
 * Class BuildResultAction
 *
 * @package DicomResearch\ResultBuildBundle\BuildResult
 */
class BuildResultAction
{
    /**
     * @var ClientOfIssueTrackerAbstract[]
     */
    private $clients;

    public function __construct(array $clients)
    {
        $this->clients = $clients;
    }

    /**
     * @return array|\DicomResearch\ResultBuildBundle\Client\ClientOfIssueTrackerAbstract[]
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * @param array $actions
     * @param BuildResult $buildResult
     *
     * @throws BuildResultActionsException
     */
    public function doActionsAfterBuilds(array $actions, BuildResult $buildResult)
    {
        foreach ($actions as $command => $param) {
            foreach ($this->getClients() as $client) {
                if ($client->canDoCommand($command)) {
                    $client->doCommand($command, $buildResult, $param);

                    continue 2;
                }
            }

            throw BuildResultActionsException::unknownActionCommand($command);
        }
    }
}
