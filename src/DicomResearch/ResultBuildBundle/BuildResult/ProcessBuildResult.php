<?php
/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 07.08.15
 * Time: 15:56
 */

namespace DicomResearch\ResultBuildBundle\BuildResult;

class ProcessBuildResult
{
    /**
     * @param BuildResult $buildResult
     *
     * @return array
     */
    public function getActionsAtBuildResult(BuildResult $buildResult)
    {
        if ($buildResult->getStateDescription() !== 'finished') {
            return [];
        }

        //todo на будущее, нужно сделать команду объектом,
        return [
            'updateRedmineIssue' => [
                'status'    => $buildResult->getBuildResult() === 'success' ? 'Resolved' : 'In Progress',
                'notes'     => $buildResult->getStatusHtml()
            ]
        ];
    }
}
