<?php
/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 05.08.15
 * Time: 11:14
 */

namespace DicomResearch\ResultBuildBundle\BuildResult;

/**
 * Класс, хранящий в себе данные из результата сборки
 *
 * Class BuildResult
 *
 * @package DicomResearch\ResultBuildBundle\BuildResult
 */
class BuildResult
{
    /**
     * Название проекта
     *
     * @var string
     */
    private $projectName;

    /**
     * Ветка которую проверяли
     *
     * @var string
     */
    private $branchName;

    /**
     * Номер таска
     *
     * @var integer
     */
    private $issueId;

    /**
     * Результат сборки
     *
     * @var string
     */
    private $buildResult;

    /**
     * Шаг сборки
     *
     * @var string
     */
    private $stateDescription;

    /**
     * Описание в формате HTML
     *
     * @var string
     */
    private $statusHtml;

    /**
     * @param string $projectName
     *
     * @return mixed
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;
        return $this->projectName;
    }

    /**
     * @return string
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * @param string $branchName
     *
     * @return mixed
     */
    public function setBranchName($branchName)
    {
        $this->branchName = $branchName;
        return $this->branchName;
    }

    /**
     * @return string
     */
    public function getBranchName()
    {
        return $this->branchName;
    }

    /**
     * @param integer $issueId
     *
     * @return mixed
     */
    public function setIssueId($issueId)
    {
        $this->issueId = $issueId;
        return $this->issueId;
    }

    /**
     * @return string
     */
    public function getIssueId()
    {
        return $this->issueId;
    }

    /**
     * @param string $buildResult
     *
     * @return mixed
     */
    public function setBuildResult($buildResult)
    {
        $this->buildResult = $buildResult;
        return $this->buildResult;
    }

    /**
     * @return string
     */
    public function getBuildResult()
    {
        return $this->buildResult;
    }

    /**
     * @param string $stateDescription
     *
     * @return mixed
     */
    public function setStateDescription($stateDescription)
    {
        $this->stateDescription = $stateDescription;
        return $this->stateDescription;
    }

    /**
     * @return string
     */
    public function getStateDescription()
    {
        return $this->stateDescription;
    }

    /**
     * @param string $statusHtml
     *
     * @return mixed
     */
    public function setStatusHtml($statusHtml)
    {
        $this->statusHtml = $statusHtml;
        return $this->statusHtml;
    }

    /**
     * @return string
     */
    public function getStatusHtml()
    {
        return $this->statusHtml;
    }
}
