<?php
/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 07.08.15
 * Time: 15:20
 */

namespace DicomResearch\ResultBuildBundle\Exception;

class TicketNumberResolvedException extends \Exception
{
    public static function unknownProjectName($projectName)
    {
        return new static('Unknown project name "' . $projectName . '"');
    }
}
