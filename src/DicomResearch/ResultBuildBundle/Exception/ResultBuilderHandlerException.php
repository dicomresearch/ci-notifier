<?php

/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 05.08.15
 * Time: 12:54
 */
namespace DicomResearch\ResultBuildBundle\Exception;

class ResultBuilderHandlerException extends \Exception
{
    public static function builderMustBeDefined()
    {
        return new static('You must define "builder"');
    }
}
