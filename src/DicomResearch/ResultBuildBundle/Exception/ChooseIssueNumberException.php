<?php
/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 07.08.15
 * Time: 14:29
 */

namespace DicomResearch\ResultBuildBundle\Exception;

class ChooseIssueNumberException extends \Exception
{
    public static function errorNameBranch($branchName)
    {
        return new static('The name of the branch "' . $branchName . '" is not valid');
    }
}
