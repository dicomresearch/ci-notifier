<?php
/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 10.08.15
 * Time: 11:42
 */

namespace DicomResearch\ResultBuildBundle\Exception;

class BuildResultActionsException extends \Exception
{
    public static function unknownActionCommand($command)
    {
        return new static('Unknown action command ' . $command);
    }
}
