<?php
/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 05.08.15
 * Time: 13:47
 */

namespace DicomResearch\ResultBuildBundle\Exception;

class TeamCityBuilderException extends \Exception
{
    /**
     * @param $parameter
     * @param $teamCityBuildResult
     *
     * @return static
     */
    public static function notFoundParameter($parameter, $teamCityBuildResult)
    {
        return new static(
            'Parameter ' . $parameter . ' not found in post params. Params came: ' . print_r($teamCityBuildResult, true)
        );
    }
}
