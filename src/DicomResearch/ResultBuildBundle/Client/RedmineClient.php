<?php
/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 06.08.15
 * Time: 14:08
 */

namespace DicomResearch\ResultBuildBundle\Client;

use DicomResearch\ResultBuildBundle\BuildResult\BuildResult;
use Redmine\Client;

/**
 * Class RedmineClient
 *
 * @package DicomResearch\ResultBuildBundle\Client
 */
class RedmineClient extends ClientOfIssueTrackerAbstract
{
    /**
     * Список команд, которые можно выполнить
     *
     * @var array
     */
    private $commands = [
        'updateRedmineIssue'
    ];

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $apiKey;

    public function __construct($url, $apiKey)
    {
        $this->url = $url;
        $this->apiKey = $apiKey;

        $client = new Client($this->url, $this->apiKey);
        $this->setClient($client);

    }

    /**
     * @return array
     */
    public function getCommands()
    {
        return $this->commands;
    }

    /**
     * @param $projectName
     * @param $issueId
     * @param $params
     * @return void
     */
    public function update($projectName, $issueId, $params)
    {
        $this->getClient()->api('issue')->update($issueId, $params);
    }

    /**
     * @param string $commandName
     *
     * @return bool
     */
    public function canDoCommand($commandName)
    {
        return in_array($commandName, $this->getCommands());
    }

    /**
     * @param $commandName
     * @param BuildResult $buildResult
     * @param $param
     *
     * @return void
     */
    public function doCommand($commandName, BuildResult $buildResult, $param)
    {
        switch ($commandName) {
            case 'updateRedmineIssue':
                $this->update($buildResult->getProjectName(), $buildResult->getIssueId(), $param);
                break;
        }
    }
}
