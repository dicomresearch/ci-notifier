<?php

/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 06.08.15
 * Time: 14:06
 */

namespace DicomResearch\ResultBuildBundle\Client;

use DicomResearch\ResultBuildBundle\BuildResult\BuildResult;

/**
 * Обертка над клиентами для работы
 * с разными системами учета задач
 *
 * Class ClientOfIssueTrackerAbstract
 *
 * @package DicomResearch\ResultBuildBundle\Client
 */
abstract class ClientOfIssueTrackerAbstract
{
    /**
     * Клиент для работы с API issue трекера
     *
     * @var mixed
     */
    private $client;

    /**
     * Обновление задачи
     *
     * @param $projectName
     * @param $issueId
     * @param $params
     *
     * @return mixed
     */
    abstract public function update($projectName, $issueId, $params);

    /**
     * @param $client
     *
     * @return mixed
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this->client;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Может ли клиент выполнить команду
     *
     * @param string $commandName
     *
     * @return bool
     */
    abstract public function canDoCommand($commandName);

    /**
     * @param $commandName
     * @param $param
     * @param BuildResult $buildResult
     *
     * @return mixed
     */
    abstract public function doCommand($commandName, BuildResult $buildResult, $param);
}
