<?php
/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 07.08.15
 * Time: 11:40
 */

namespace DicomResearch\ResultBuildBundle\Issue;

use DicomResearch\ResultBuildBundle\Exception\TicketNumberResolvedException;

/**
 * Class TicketNumberResolver
 *
 * @package DicomResearch\ResultBuildBundle\Issue
 */
class TicketNumberResolver
{
    /**
     * @var array [projectName => ChooseIssueNumberInterface]
     */
    private $chooseIssueNumberMap = [
        'WorkflowEngine' => 'DicomResearch\ResultBuildBundle\Issue\ChooseIssueNumberStrategy\ChooseIssueNumberOfFront'
    ];

    /**
     * @param $branchName
     * @param $projectName
     *
     * @return mixed
     */
    public function getTicketNumberFromBranchName($branchName, $projectName)
    {
        $chooseIssueNumberClass = $this->getChooseIssueNumberClass($projectName);
        $chooseIssueNumber = new $chooseIssueNumberClass();

        $issueId = $chooseIssueNumber->chooseIssueNumber($branchName);

        return $issueId;
    }

    /**
     * @param $projectName
     *
     * @return mixed
     * @throws TicketNumberResolvedException
     */
    public function getChooseIssueNumberClass($projectName)
    {
        if (!array_key_exists($projectName, $this->chooseIssueNumberMap)) {
            throw TicketNumberResolvedException::unknownProjectName($projectName);
        }

        return $this->chooseIssueNumberMap[$projectName];
    }
}
