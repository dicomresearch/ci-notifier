<?php
/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 07.08.15
 * Time: 13:37
 */

namespace DicomResearch\ResultBuildBundle\Issue\ChooseIssueNumberStrategy;

use DicomResearch\ResultBuildBundle\Exception\ChooseIssueNumberException;

/**
 * Номер таска в начале названия ветки.
 * Имя ветки по шаблону issueId_feature|bugfix_description
 *
 * Class ChooseIssueNumberOfFront
 *
 * @package DicomResearch\ResultBuildBundle\Issue\ChooseIssueNumberStrategy
 */
class ChooseIssueNumberOfFront implements ChoseIssueNumberInterface
{
    /**
     * @param string $branchName example, 123_feature_create_user
     *
     * @return mixed
     * @throws ChooseIssueNumberException
     */
    public function chooseIssueNumber($branchName)
    {
        $branchNameAsArray = explode('_', $branchName);

        if (count($branchNameAsArray) <= 1 || !is_numeric($branchNameAsArray[0])) {
            throw ChooseIssueNumberException::errorNameBranch($branchName);
        }

        return $branchNameAsArray[0];
    }
}
