<?php
/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 07.08.15
 * Time: 13:32
 */

namespace DicomResearch\ResultBuildBundle\Issue\ChooseIssueNumberStrategy;

interface ChoseIssueNumberInterface
{
    /**
     * @param string $branchName
     *
     * @return mixed
     * @throws ChooseIssueNumberException
     */
    public function chooseIssueNumber($branchName);
}
